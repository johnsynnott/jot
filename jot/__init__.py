import datetime
import discord
import importlib


class Jot:
    def __init__(self, config=None):
        self.commands = {}
        self.config = {} if config is None else config
        self.client = discord.Client()

        @self.client.event
        async def on_message(message):
            # Don't react to our own messages
            if message.author == self.client.user:
                return

            if message.content.startswith('!'):
                print("[{}] [{}]\tcommand: {}".format(datetime.datetime.now(), message.author, message.content))
                split = message.content.split(' ', 1)
                await self.client.send_message(
                    message.channel,
                    await self.commands[split[0][1:].lower()](
                        self.client,
                        message,
                        split[1] if len(split) > 1 else None
                    )
                )

    def run(self):
        for package in self.config.get('command_packages', []):
            self.commands.update(importlib.import_module(package).commands)
        self.commands.update(self.config.get('commands', {}))
        self.client.run(self.config['client-token'])
