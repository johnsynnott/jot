import asyncio
import time


async def user_hello(client, message, payload):
    return "Hello, {}".format(message.author)


async def echo(client, message, payload):
    return payload


async def sleep(client, message, payload):
    # time.sleep(float(payload))
    await asyncio.sleep(float(payload))
    return "Slept for {} seconds".format(float(payload))


commands = {
    'hi': user_hello,
    'echo': echo,
    'sleep': sleep
}

