from pprint import pprint as pp


async def debug(client, message, payload):
    pp(dir(client))
    pp(dir(message))
    pp(dir(list(message.server.members)[0]))
    return "{}".format(list(message.server.members))


async def test(client, message, payload):
    server = message.server
    return str([(x.id, server.id) for x in server.members])


commands = {'debug': debug, 'd': test}