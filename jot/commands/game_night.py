import aiosqlite


async def init_tables():
    async with aiosqlite.connect('game_night.db') as db:
        await db.execute("CREATE TABLE IF NOT EXISTS servers (id INTEGER PRIMARY KEY UNIQUE);")
        await db.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY UNIQUE, server INTEGER REFERENCES servers(id));")
        await db.commit()


async def populate_tables(server):
    async with aiosqlite.connect('game_night.db') as db:
        await db.execute("INSERT INTO servers (id) VALUES (?)", [server.id])
        await db.executemany("INSERT INTO users (id, server) VALUES (?, ?)", [(x.id, server.id) for x in server.members])
        await db.commit()


async def game_night_root(client, message, payload):
    print(payload)
    await init_tables()
    await populate_tables(message.server)
    try:
        split = payload.split(' ', 1)
        return await gn_commands[split[0]](client, message, split[1] if len(split) > 1 else None)
    except (KeyError, IndexError, AttributeError):
        return "This is where a generic response would go"


async def cook_root(client, message, payload):
    if payload is None:
        return "This is where the next cook would go"
    else:
        return "This is where the logic for the commands would go"


gn_commands = {
    'cook': cook_root
}

commands = {'gn': game_night_root}



